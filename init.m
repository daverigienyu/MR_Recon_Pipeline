% ============================================================================
% //-------------------------- Setup Paths ---------------------------------//
% ............................................................................

global CONFIG
CONFIG = YAML.read('config.yml');

% ============================================================================ 

thisdir = fileparts(mfilename('fullpath'));
addpath(genpath(fullfile(thisdir, 'Src')));

addpath(genpath(CONFIG.paths.mr_recon_path));
addpath(genpath(CONFIG.paths.gpunufft_path));
