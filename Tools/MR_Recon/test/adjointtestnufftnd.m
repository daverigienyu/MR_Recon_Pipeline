%% ________________________________________________________________________
%                        Creates NUFFT Operator                         
%__________________________________________________________________________

    clc; clear;

    nRow = 128;
    nCol = 128;
    nSlice = 48;
    nResp = 5;
    nCard = 5;
    
    x = rand([nRow,nCol,nSlice,nResp,nCard]); % 64^3 image with 5 resp & 5 card gates
    x = complex(rand(size(x)), rand(size(x)));
    imageDim = size(x);
    nSlice = imageDim(3);
    
    nFE  = 512;
    nPar  = 37;
    nView = 50;
    nCoil = 1;
    
    kdataSize = [nFE, nPar, nView, nCoil];
    y = complex(rand(kdataSize), rand(kdataSize));
    
    [traj, dens] = goldenAngleStackOfStars(nFE, nSlice, nView, nPar);
    
    g1 = randi(nResp,[nPar*nView,1]);
    g2 = randi(nCard, size(g1));
    
    gatingSignal2d = [g1,g2];
    
    [kdataArr, trajArr, densArr] = sortGatedKdata(y, traj, dens, gatingSignal2d);
    
    sensMap = ones([nRow, nCol, nSlice, nCoil, nResp, nCard]);
    
    FT = gpuNUFFTND(trajArr, densArr, sensMap);
    
        
    
%==========================================================================

%% ________________________________________________________________________
%                           Check Operator Adjoint                         
%__________________________________________________________________________

N = 3;

for i=1:N
   FT.checkOperatorAdjoint(); 
end


%==========================================================================