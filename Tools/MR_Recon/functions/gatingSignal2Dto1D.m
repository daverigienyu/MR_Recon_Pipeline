function gatingSignal1d = gatingSignal2Dto1D(gatingSignal2d)

    idx = (gatingSignal2d < 1);
    
    gatingSignal2d(idx) = nan;
    
    nGates = max(gatingSignal2d, [], 1); %2d matrix e.g. [nResp, nCard]
    gatingSignal1d = sub2ind(nGates, gatingSignal2d(:,1), gatingSignal2d(:,2));

    idx = (gatingSignal1d == nan);
    gatingSignal1d(idx) = -1;
    

end

