function [new_gating_signal, gates] = main(gating_signal, nGates)

    % This is a simple wrapper that handles NaN values
    
    idx = ~isnan(gating_signal);
        
    new_gating_signal   = 0.0*gating_signal;
    
    [new_gating_signal(idx), gates] = createGatingSignal1d_helper(gating_signal(idx), nGates);
    
    new_gating_signal(~idx) = -1;
     
    

end

function [new_gating_signal, gates] = createGatingSignal1d_helper(gating_signal, nGates)

    if (nargin ~= 2)
        disp('[new_gating_signal] = SortGatingSignalAmpl(gating_signal,gates)');
    else


    nMeas     = length(gating_signal);
    chunkSize = floor(nMeas/nGates);

    % Sort data 
    [gs_sorted,idx] = sort(gating_signal);

    % Get indices to revert back to unsorted state
    [~, idx_rev] = sort(idx);

    % Assign gate numbers by partitioning values evenly into discrete number of
    % gates
    for i = 0:1:(nGates-1)
       iLeft  = 1 + i*chunkSize;
       if i == (nGates - 1)
           iRight = length(gating_signal);
       else
           iRight = iLeft + (chunkSize - 1);
       end
       gs_sorted(iLeft:iRight) = (i+1);
    end

    %cutLength = round(nMeas/100);
    %gs_sorted(1:cutLength) = -1;
    %gs_sorted((end-cutLength):end) = -1;


    % unsort the data
    new_gating_signal = gs_sorted(idx_rev);
    
    gates = max(new_gating_signal);

    
end


end