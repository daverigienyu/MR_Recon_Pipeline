function [traj3d, densityCompen3d] = goldenAngleStackOfStars_old(nCol, nSlice, nView, nPar);
% Modified by David Rigie
% nPar specifies what is actually measured, and nSlice is the total
% number of partitions that will be reconstructed
% nPar < nSlice due to partial fourer

convert = @(x) single(x);
%convert = @(x) double(x);

Gn = (1 + sqrt(5))/2;
GA=180/Gn;
rho = (-nCol/2 : nCol/2-1);rho=rho+0.5;
rho=rho/nCol;rho=rho';
phi=zeros(1,nSlice*nView);

for n = 1:nSlice*nView
    phi(n) = mod(GA*floor((n-1)/nSlice),360)/180*pi;
end

rho_z = (nSlice/2:-1:-nSlice/2+1)-0.5;
rho_z=rho_z/max(abs(rho_z(:)))*nSlice/2;
rho_z=rho_z/nSlice;rho_z=rho_z';

rho_z=repmat(rho_z,[1,nView]);
rho_z=reshape(rho_z,[1,nSlice*nView]);

kx = -rho*sin(phi);  
ky = rho*cos(phi); 
kz = repmat(rho_z,[nCol,1]); 

clear Traj3D
traj3d(:,:,:,1)=convert(reshape(kx,[nCol,nSlice,nView,1]));
traj3d(:,:,:,2)=convert(reshape(ky,[nCol,nSlice,nView,1]));
traj3d(:,:,:,3)=convert(reshape(kz,[nCol,nSlice,nView,1]));

% Extract part that is actually measured
traj3d = traj3d(:,nSlice-nPar+1:nSlice,:,:);
traj3d = reshape(traj3d, [], 3);

densityComp = abs(rho);
densityComp=densityComp/max(densityComp(:));
densityCompen3d = convert(repmat(densityComp,[1,nSlice,nView]));

% Extract part that is actually measured
densityCompen3d = densityCompen3d(:,nSlice-nPar+1:nSlice,:);
densityCompen3d = densityCompen3d(:);



