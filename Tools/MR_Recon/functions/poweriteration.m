function specNorm = poweriteration(A, AT, x0, nIter)
% Computes the spectral norm of operator A using power iteration.
% A and AT should be specified as function handles.

    if nargin < 4
        warning('Defaulting to 10 iterations');
        nIter = 10;
    end

    x = randlike(x0);
    
    fprintf('\n\n');
    for i = 1:1:nIter
        x = AT(A(x));
        x = x./norm(x(:));
        v = A(x);
        s = norm(v(:));
        fprintf('\nIter %i, ||A|| = %f',i,s);
        pause(0.0001);
    end
    fprintf('\n\n');
    
    specNorm = s;
    
end



function x = randlike(x)

    if isreal(x)
        x = rand(size(x));
    else
        x = rand(size(x)) + i*rand(size(x));
    end

end
