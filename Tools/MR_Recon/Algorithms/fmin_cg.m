function [xopt, convergenceInfo] = fmin_cg(Afun, x0, r0, varargin)
% Linear CG for solving Ax = y
% Note that A must be real and symmetric. Normally this is used to solve
% something like 1/2||Ax-y||_2^2 --> A'Ax = A'y
%_________________________________________________________________________%
%                             Parse Inputs                                %
%=========================================================================%
p = inputParser;
p.addRequired('Afun');
p.addRequired('x0', @isnumeric);
p.addRequired('r0', @isnumeric);
p.addOptional('maxiter', 5, @(x) floor(x)==x);
p.addOptional('callback', @(varargin) disp(''), @(h) isa(h, 'function_handle'));
p.addOptional('callbackArgs', {}, @isstruct);
p.addOptional('resTol', 1e-12, @isscalar);
p.addOptional('verbose', false, @islogical);

parse(p, Afun,  x0, r0, varargin{:});

pFields = fields(p.Results);

% Unpack input parser results to variables names in function scope, e.g.
% varName = p.Results.varName
for i = 1:1:numel(pFields)
    eval(sprintf('%s = p.Results.%s;', pFields{i}, pFields{i}));
end

%_________________________________________________________________________%

    % Initialize
    iter = 0;
    x    = x0;
    r    = r0;
    p    = -r;
  
    while iter < maxiter
           
        resNorm = norm(r(:),2);
        if resNorm < resTol
            break;
        end       
        
        % Following the notation in nocedal and wright
        Ap = Afun(p);
        
        alpha  = vdot(r,r)/(vdot(p,Ap));
        x      = x + alpha*p;
        
        r_last = r;
        r      = r + alpha*Ap;
        
        beta   = vdot(r,r)/vdot(r_last,r_last);
        
        p      = -r + beta*p;       
        
        % Done with iteration
        iter = iter + 1;
        
        dxNorm = norm(alpha*p,2); % change from previous iteration
        resNorm = sqrt(vdot(r,Afun(r)));
        
        % Execute callback
        callback(x, callbackArgs{:});
        
        if verbose
            dispProgress(iter, resNorm, dxNorm);
        end
        
    end
    
    xopt = x;

    if verbose
        fprintf('\n\n');
    end
    
end

function [] = dispProgress(iter, resNorm, dxNorm)

    fprintf('\r[Iter: %i, Residual: %6E, dx: %6E]', ...
             iter, resNorm, dxNorm);
    
end



function val = vdot(x,y)

    val = dot(x(:),y(:));

end
