function Xnew = regridarray(X, newDims, varargin)
%REGRIDARRAY interpolate array to new dimensions
%
%   regridarray(X, newDims) interpolates array X to newDims, where newDims
%   is an array, such that size(regridarray(X, newDims)) = newDims
%
%   newDims = [256, -1, 256] means keep the second dimension the same
%
%   optional input varargin can be used to control the interpolation style.
%   See interp1 for more.
    
    oldDims      = size(X);
    idx          = (newDims == -1);
    newDims(idx) = oldDims(idx);

    % If X is complex, do magnitude and phase separately and then rejoin
    if ~isreal(X)
        disp('Regridding magnitude and phase separately');
        R = regridarray(abs(X), newDims, varargin);
        theta = regridarray(angle(X), newDims, varargin);
        Xnew = R.*exp(i*theta);
        return;
    end

    Xnew = X;
    
    % Handle each dim separately
    for iDim = 1:numel(newDims)
       nPoints = newDims(iDim);
       Xnew = regridarray_1d(Xnew, nPoints, iDim, varargin{:});                
    end

end

function Xnew = regridarray_1d(X, num_points, dim, varargin)

    

    order = 1:1:ndims(X);
    order = circshift(order,[0,-(dim-1)]);
    X_temp = permute(X,order);

    x0 = linspace(0,1,size(X_temp,1))';
    xnew = linspace(0,1,num_points)';

    Xnew = interp1(x0, X_temp, xnew, varargin{:});

    Xnew = ipermute(Xnew, order);
    

end