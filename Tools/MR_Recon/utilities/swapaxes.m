function [y] = swapaxes(x, ind1, ind2)

    % indices in normal order
    ii = 1:1:ndims(x);
    
    % swap positions of ind1 and ind2
    temp = ii(ind1);
    ii(ind1) = ind2;
    ii(ind2) = temp;
    
    % output
    y = permute(x, ii);

end

