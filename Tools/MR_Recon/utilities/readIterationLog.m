function [iterArr, objArr, fig] = readIterationLog(filepath)

    s = fileread(filepath);

    matches = regexp(s, '(\d+)\s+,\s+obj:\s*([\d\.])*', 'tokens');

    x = zeros([numel(matches), 1]);
    y = x*1.0;
    for i = 1:1:numel(matches)
       iter = str2num(matches{i}{1});
       obj  = str2num(matches{i}{2});
       x(i) = iter + 1;
       y(i) = obj;
    end

    %plot(x,y);
    fig = figure;
    plot(x,y, 'Linewidth', 2.0);
    axis square;
    xlabel('iteration');
    ylabel('objective function value');
    
    iterArr = x;
    objArr  = y;
    
end
    