function y = div1d(x, dim, dx)

    if nargin < 3
        dx = 1.0;
    end

    % Compute slice indices for arbitrary dimensions
    D         = ndims(x);
    N         = size(x,dim);
    indexing1 = repmat({':'},[1,D]);
    indexing2 = indexing1;
    indexing3 = indexing1;
    
    indexing1{dim} = [1,1:(N-1)]; indexing2{dim} = [1:N];
    y              = x(indexing1{:}) - x(indexing2{:});
    
    indexing1{dim}  = 1;
    y(indexing1{:}) = -x(indexing1{:});

    indexing1{dim} = N; indexing2{dim} = N-1; indexing3{dim} = N-2;
    y(indexing2{:}) = x(indexing1{:})- x(indexing2{:}) + x(indexing3{:});
    
    indexing1{dim} = N; indexing2{dim} = N-1;
    y(indexing1{:}) = x(indexing2{:})- x(indexing1{:});
    
    y = -1*y/dx;
end

