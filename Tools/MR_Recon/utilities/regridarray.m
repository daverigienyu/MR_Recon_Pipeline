function Xnew = regridarray(X, newDims)
%REGRIDARRAY interpolate array to new dimensions
%
%   regridarray(X, newDims) interpolates array X to newDims, where newDims
%   is an array, such that size(regridarray(X, newDims)) = newDims
%
%   newDims = [256, -1, 256] means keep the second dimension the same


    oldDims = size(X);
    idx          = (newDims == -1);
    newDims(idx) = oldDims(idx);
    
    if newDims ~= round(newDims)
        warning('rounding dimensions to integer values');
        newDims = round(newDims);
    end
    
    if ~isreal(X)
        disp('Regridding magnitude and phase separately');
        R = regridarray(abs(X), newDims);
        theta = regridarray(angle(X), newDims);
        Xnew = R.*exp(i*theta);
        return;
    end

    
    for iDim = 1:1:ndims(X)
        gridvectors{iDim} = linspace(0, 1, oldDims(iDim));
    end
    
    F = griddedInterpolant(gridvectors, X);
    
    for iDim = 1:1:numel(newDims)
       gridvectors{iDim} = linspace(0, 1, newDims(iDim));        
    end
    
    Xnew = F(gridvectors); 

end