function varargout = vdot(x,y, varargin)
% Behaves like norm function but treats x as 1d vector regardless of shape

    varargout{:} = dot(x(:), y(:), varargin{:});

end