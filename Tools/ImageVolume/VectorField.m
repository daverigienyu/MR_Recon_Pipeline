classdef VectorField < ImageVolume

    properties
        U
        V
        W
    end

    properties(SetAccess=private)
        vectorStorage 
    end

    methods

       function obj = VectorField(IV)

            if ischar(IV) && exist(IV,'file')
                IV = ImageVolumeNifti(IV);
            end

            obj = obj@ImageVolume(IV.voxelData, IV.origin, ...
                                  IV.directionCosines, IV.voxelSpacing,...
                                  IV.dims);

            for f = fields(IV)'
                try 
                    obj = setfield(obj, f{1}, getfield(IV,f{1}));
                catch ME
                    ; 
                end
            end
            
            obj.vectorStorage = obj.coordinateSystem;
            
          end

       function U = get.U(obj)
            U = obj.voxelData(:,:,:,1);
            if ~strcmpi(obj.vectorStorage, obj.coordinateSystem)
                U = -1.0*U;
            end
       end

       function V = get.V(obj)
            V = obj.voxelData(:,:,:,2);
            if ~strcmpi(obj.vectorStorage, obj.coordinateSystem)
                V = -1.0*V;
            end
       end
       
       function W = get.W(obj)
            W = obj.voxelData(:,:,:,3);
       end 

       function varargout = regridvectors(obj, varargin)

           if  isnumeric(varargin{1})
               newdims = varargin{1}
               varargout{1} = regridarray(obj.U, newdims);
               varargout{2} = regridarray(obj.V, newdims);
               varargout{3} = regridarray(obj.W, newdims);
           else
               IVout               = VectorField(varargin{1});
               IVout.modality      = obj.modality;
               [U,V,W, interpolator]             = obj.regridvectors2match(varargin{1});
               %IVout.voxelData = cat(4,U,V,W);
               
                for k = 1:3
                    IVout.voxelData(:,:,:,k) = interpolator(obj.voxelData(:,:,:,k));
                end

               IVout.vectorStorage = obj.vectorStorage;
               varargout{1} = IVout;
           end

       end

       function [U,V,W,interpolator] = regridvectors2match(obj, IVout)
            temp = obj;
            temp.voxelData    = temp.U;
            [U, interpolator] = temp.regridvolume2match(IVout);
            V = interpolator(obj.V);
            W = interpolator(obj.W);

       end

       function [Ui,Vi,Wi] = getIndexedVectorfield(obj)
            % Returns vector field in (r,c,s) units instead of real world
            % units

            Sinv = obj.Sinv;
            Sinv(4,:) = 0.0;
            Sinv(:,4) = 0.0; % We don't care about the displacement part for transforming vector fields;

            [Ui,Vi,Wi] = applyAffineTransform(Sinv, obj.U,...
                                                    obj.V,...
                                                    obj.W);

       end

       

    end

    methods(Static)

        function [U,V,W] = reorient(U, V, W, orient_in, orient_out)

            axis_order = decodeOrientationTransform(orient_in,orient_out);
            
            U = ImageVolume.reorient(U, orient_in, orient_out);
            V = ImageVolume.reorient(V, orient_in, orient_out);
            W = ImageVolume.reorient(W, orient_in, orient_out);

            Rot = perm2mat(axis_order);
            
            [U,V,W] = applyRotation(Rot, U, V, W);

        end

    end


end