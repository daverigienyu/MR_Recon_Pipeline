function [ord, permInds] = mat2orientation(M)

    ord = '';
    for i = 1:size(M,2)
        ord = strcat(ord, vec2axis_lps(M(:,i)));
    end

    coord.L = 1;
    coord.P = 2;
    coord.S = 3;
    
    coord.R = -1;
    coord.A = -2;
    coord.I = -3;

    permInds = [];

    for c = ord
        permInds = cat(2,permInds,getfield(coord, c));
    end

end

function lbl = vec2axis_lps(vec)

        vec = vec/norm(vec(:));
        if isequal(vec(:)', [1,0,0])
                lbl = 'L';
        elseif isequal(vec(:)', [-1,0,0])
                lbl = 'R';
        elseif isequal(vec(:)', [0,1,0])
                lbl = 'P';
        elseif isequal(vec(:)', [0,-1,0])
                lbl = 'A';
        elseif isequal(vec(:)', [0,0,1])
                lbl = 'S';
        elseif isequal(vec(:)', [0,0,-1])
                lbl = 'I';
        else
            error('Uconventional ordering');

        end
end