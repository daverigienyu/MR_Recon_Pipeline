function M = perm2mat(axis_order)

    M = zeros(length(axis_order));

    for i = 1:1:numel(axis_order)
        M(abs(axis_order(i)), i) = sign(axis_order(i));
    end

end