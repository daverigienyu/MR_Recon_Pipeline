function [x,y,z] = applyAffineTransform(A, x0, y0, z0)

    % Applies the affine transformation A to all points x0,y0,z0
    % A could be either 4x4 or 3x4, x0,y0,z0 can be any shape or dimension
    % and this will be preserved in the output

    x = A(1,1).*x0 + A(1,2).*y0 + A(1,3).*z0 + A(1,4);
    y = A(2,1).*x0 + A(2,2).*y0 + A(2,3).*z0 + A(2,4);
    z = A(3,1).*x0 + A(3,2).*y0 + A(3,3).*z0 + A(3,4);    

    
end