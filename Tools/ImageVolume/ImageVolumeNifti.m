classdef ImageVolumeNifti < ImageVolumeAffine
   
    properties
    end
    
    methods
       
        function obj = ImageVolumeNifti(niftipath)
           
             NiftiObj  = load_untouch_nii(niftipath);
             %NiftiObj  = load_nii(niftipath);
             voxelData = double(NiftiObj.img);
             
             dims      = size(voxelData);
             
             S_ras = eye(4);
             S_ras(1,:) = NiftiObj.hdr.hist.srow_x; % The minus sign is because NIFTI uses RAS instead of LPS
             S_ras(2,:) = NiftiObj.hdr.hist.srow_y;
             S_ras(3,:) = NiftiObj.hdr.hist.srow_z;
             S_lps = RAS2LPS*S_ras;
             
             
             
             obj = obj@ImageVolumeAffine(voxelData, S_lps, dims);
             obj.coordinateSystem = 'RAS';
             
            
        end
        
    end
    
    
    
end