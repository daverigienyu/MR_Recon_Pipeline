function B = perm(A, order)

    numDims = ndims(A);
    order = order(1:numDims);

    flipdims = sign(order);

    B = permute(A, sign(order).*order);
    
    idx = find(flipdims == -1);

    for i = idx(:)'
        B = flipdim(B,i);
    end


end