classdef ImageVolumeTwix < ImageVolume
    % Encapsulates geometry information to position image voxels in space
    % 
    % This resource is excellent for summarizing DICOM geometry
    % http://nipy.org/nibabel/dicom/dicom_orientation.html
    properties
        hdr
    end
    
    methods
        
        function obj = ImageVolumeTwix(voxelData, varargin)
            % ImageVolumeTwix(voxelData, twixPath)
            % ImageVolumeTwix(voxelData, twix_obj)
            
            % guess if second argument is twix path or twix_obj based
            % on length
            if isstruct(varargin{1})
                twix_obj = varargin{1};
                hdr = createMiniHeaderFromTwix(twix_obj);
            else
                twixpath = varargin{1}
                hdr = createMiniHeader(twixpath, false);
            end
            
            imagePlane = ImageVolumeTwix.detectImagePlane(hdr.normalVector);
            fprintf('\nAutodetected imaging plane is %s', imagePlane);
            
            % Guess direction cosine matrix
            % https://groups.google.com/forum/#!topic/comp.protocols.dicom/GW04ODKR7Tc
            DCM = ImageVolumeTwix.setDefaultDCM(imagePlane);
            DCM = DCM(:,[2,1,3]);
                            
            fov  = [hdr.fov.read
                    hdr.fov.phase
                    hdr.fov.slice];
                
            dims = [hdr.ImageSize.nRow
                    hdr.ImageSize.nCol
                    hdr.ImageSize.nSlice];
                
            voxelSpacing = fov(:)./dims;
                       
            
            offsetXYZ    = [hdr.offset.dSag 
                            hdr.offset.dCor 
                            hdr.offset.dTra];
                        
            tablePosXYZ  = [hdr.globalTablePos.dSag 
                            hdr.globalTablePos.dCor
                            hdr.globalTablePos.dTra];
            
            % Table position and half pixel shift in plane
            center = offsetXYZ + tablePosXYZ - DCM*([0.5;0.5;0.0].*voxelSpacing);       
            origin = center - 0.5*DCM*((dims-1).*voxelSpacing);               
            
                                     
            obj = obj@ImageVolume(voxelData, origin, DCM, voxelSpacing, dims); 
  
            
            obj.hdr = hdr;
            obj.modality = 'MR';
        end
        
    end
    
    methods(Static)
       
        function imagePlane = detectImagePlane(normalVector)
            
            vec = [normalVector.dSag; normalVector.dCor; normalVector.dTra];
            
            if all(vec(:) == [1;0;0])
                imagePlane = 'SAGITTAL';
            elseif all(vec(:) == [0;1;0])
                imagePlane = 'CORONAL';
            elseif all(vec(:) == [0;0;1])
                imagePlane = 'AXIAL';
            else
                warning('Unable to autodetect imaging plane from normalVector %s', mat2str(vec));
                warning('Defaulting to Sagittal');
                imagePlane = 'SAGITTAL'; 
            end
            
        end
        
        function DCM = setDefaultDCM(imagePlane)
           
            L = [1;0;0];
            P = [0;1;0];
            H = [0;0;1];
            
            switch imagePlane
                case 'SAGITTAL'
                    DCM = [P, -H, -L];
                case 'CORONAL'
                    DCM = [L, -H,  P];
                case 'AXIAL'
                    DCM = [L, P, H];
            end
            
        end
      
    end
    
    
end
    


