function axis_order = decodeOrientationTransform(orient_in, orient_out)

    % orient_in = 'RAS'  orient_out = 'LPI'

    orient_in = upper(orient_in);
    orient_out = upper(orient_out);

    RevAxis.L = 'R';
    RevAxis.P = 'A';
    RevAxis.S = 'I';
    RevAxis.R = 'L';
    RevAxis.A = 'P';
    RevAxis.I = 'S';

    axis_order = zeros([1,3]);
    
    for i = 1:numel(orient_out)
        c = orient_out(i);
        ind1 = find(orient_in == c);
        
        if ~isempty(ind1)
            axis_order(i) = ind1;
        else
            ind2 = find(orient_in == getfield(RevAxis, c));
            axis_order(i) = -ind2;
        end

    end

    if ~isequal(sort(abs(axis_order)), [1,2,3])
        error('Invalid orientation specification');
    end

end
