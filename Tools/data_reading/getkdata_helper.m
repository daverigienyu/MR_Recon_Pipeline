function [twix_obj] = getkdata_helper(twixpath, dataType, removeOS)
    
    % Determine if .DAT file or .MAT file
    [dirpath, filename, ext] = fileparts(twixpath);
    
    if strcmpi(ext, '.DAT')
        twix_obj = mapVBVD_Nav(twixpath, dataType);
        if numel(twix_obj) > 1
            twix_obj = twix_obj{end};
        end
        twix_obj.image.flagRemoveOS = removeOS;
        twix_obj = convertTwixObj(twix_obj, dataType);
    elseif strcmpi(ext, '.MAT')
        twix_obj = load(twixpath);
    elseif strcmpi(ext, '.TWIX')
        twix_obj = readTwix(twixpath);
    end
    
end



