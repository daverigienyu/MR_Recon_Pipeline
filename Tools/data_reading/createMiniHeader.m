function [head] = createMiniHeader(inputpath, outputpath)

    if nargin < 1
        [fname, pname] = uigetfile('*.dat');
        inputpath = fullfile(pname, fname);
    end
    
    if nargin < 2
        [fname, pname] = uiputfile('*.txt');
        outputpath = fullfile(pname, fname);
    end
       
    twix_obj = mapVBVD_Nav(inputpath, 'HDR');
    
    if iscell(twix_obj)
        twix_obj = twix_obj{end};
    end
    
    head = createMiniHeaderFromTwix(twix_obj);
   
    if outputpath == false
        return;
    end
        
    
    [parentdir, basename, ~] = fileparts(outputpath);
    
    fid = fopen(fullfile(parentdir, [basename, '.txt']),'w+');
    fulltext = printnestedstruct(head);
    fprintf(fid, fulltext);
    fclose(fid);
    
    save(fullfile(parentdir, [basename, '.mat']), '-struct', 'head');
    
    disp(' ');
    
end




