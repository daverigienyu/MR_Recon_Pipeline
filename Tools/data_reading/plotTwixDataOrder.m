function fig = plotTwixDataOrder(twixobj, ecoIdx)

    if nargin < 2
        ecoIdx = 'ALL';
    end

    fig = figure();
    sqzDims  = twixobj.image.sqzDims(3:end); % skip Col, Cha
    numPlots = numel(sqzDims);

    for i = 1:numPlots
        currentDim = sqzDims{i};
        ax(i) = subplot(numPlots,1,i);
        
        ydata = getfield(twixobj.image, currentDim);
        
        if strcmpi(ecoIdx,'ALL')
            ydata = ydata;
        else
            ydata = ydata(twixobj.image.Eco == ecoIdx);
        end
        
        plot(ydata, ... 
             'k-o', 'MarkerFaceColor', 'k', 'MarkerSize',3 );
        legend(currentDim);    
        if i < numPlots
            set(gca,'XTick',[]);
        end
    end
    
    xlabel('Acquisition #');
    linkaxes(ax,'x');
    
end
