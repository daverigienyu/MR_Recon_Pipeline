function headerParam = main(headerString, keyword, verbose)

    % Splits the header into lines, and first eliminates lines that don't
    % have the keyword in them to save time.

    if nargin < 3
        verbose = false;
    end

    headerString = upper(headerString);
    keyword = upper(keyword);
    
    headerLines = strsplit(headerString,'\n');
    matchingLines = strfind(headerLines, keyword);
    matchingLines = ~cellfun(@isempty, matchingLines);
    headerLines = headerLines(matchingLines);
    
    headerParams = cellfun(@(x) searchHeader(x, keyword, verbose), headerLines, 'UniformOutput', false);   
    headerParams = [headerParams{:}];
    
    % check for multiple inconsistent matches
    headerParam = unique(headerParams);
    
    if numel(headerParam) > 1
        disp('Warning! Found multiple values');
    end
    
end

function headerParam = searchHeader(headerString, keyword, verbose)

    % Regular expression queries to find the keyword and its value

    search_pattern1 = ['(\S*', keyword, '[^\n\r]*=\s*)([-+]?[\d\.]*)'];
    matches = regexpi(headerString, search_pattern1, 'tokens');
    
    search_pattern2 = ['(\S*', keyword, '[^\n\r]*<precision>\s*\d*\s*)([-+]?[\d\.]*)'];
    matches = [matches, regexpi(headerString, search_pattern2, 'tokens')];
    
    search_pattern3 = ['([^\n\r]*', keyword, '[^\n\r]*\{\s*)([-+]?\d*)\s*\}'];
    matches = [matches, regexpi(headerString, search_pattern3, 'tokens')];
        
    val_array = [];
    % unpack matches from header
    for i=1:1:numel(matches)
        match = matches{i};
        token1 = match{1};
        token2 = match{2};
        if verbose
            disp([token1,token2]);
        end
        val_array = [val_array, str2num(token2)];
    end
    
    headerParam = val_array;
    
end

