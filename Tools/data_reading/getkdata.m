function [twix_obj] = getkdata(twixpath, removeOS)

    if nargin < 2
        removeOS = false;
    end

    twix_obj = getkdata_helper(twixpath, 'IMG', removeOS);
    
end