classdef CoilAnalyzer
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        coilInfo;
        coilTable
    end
    
    methods
        function this = CoilAnalyzer(twixobj)
            hdr           = createMiniHeaderFromTwix(twixobj)
            this.coilInfo = hdr.coilInfo; 
            S = [this.coilInfo.sCoilElementID];
            
            % Add channel index
            for i = 1:numel(S)
               S(i).channel = i; 
            end
            
            % Move channel index to first column in table
            T  = struct2table(S);
            T1 = T(:,end);
            T2 = T(:,1:(end-1));
            this.coilTable  = cat(2,T1,T2);
        end 
        
        function [T, rowIdx] = search(this, varargin)
           
            % Example: this.search('body','spine');
            
            rowIdx = [];
            
            for i = 1:numel(varargin)
               [~,idx] = this.search_helper(varargin{i});
                rowIdx = [rowIdx, idx];
            end
            
            rowIdx = unique(rowIdx);
            
            T = this.coilTable(rowIdx,:);
            
           
            
        end
        
        function [T, rowIdx] = search_helper(this, query)
            
            % Search for rows containing a particular keyword
            
            rowIdx = [];
            colIdx = [];
            
            [numRows, numCols] = size(this.coilTable);
            
            strfindi = @(x,y) strfind(lower(x),lower(y));
            
            for i = 1:numRows
                for j = 1:numCols                   
                    match = strfindi(this.coilTable{i,j}, query);
                    if iscell(match)
                        match = match{1};
                    end
                    if match
                        rowIdx = [rowIdx, i];
                        break;
                    end
                end
            end
            T = this.coilTable(rowIdx,:);
        end
        
    end

end

