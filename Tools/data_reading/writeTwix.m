function [] = writeTwix(twix_obj, filepath)

    tic;
    
    [dirpath, filename, ext] = fileparts(filepath);
    ext = '.twix'; % make filename twix
    filepath = fullfile(dirpath, [filename ext]);
    
    try 
        bs = getByteStreamFromArray(twix_obj);
    catch ME
        warning(ME.message);
        warning('Data serialization seems to fail if data is > 4GB. Try reducing the data size by removing oversampling or using coil compression');
        error('Data serialization failed!!!');
    end
    

    
    % save bytestream
    fid = fopen(filepath,'wb');
    fwrite(fid, bs, 'uint8', 'ieee-le');
    fclose(fid);
        
    toc;

end