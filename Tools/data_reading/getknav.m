function [twix_obj] = getknav(twixpath, removeOS)

    if nargin < 2
        removeOS = false;
    end
    
    twix_obj = getkdata_helper(twixpath, 'NAV', removeOS);
    
end



