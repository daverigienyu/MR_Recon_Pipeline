%% Load data into twix_map_obj

twixpath = 'phantom_scan_wip991_nav_PT_0DB.dat';

%% Load unsorted data
kdata_unsorted = getkdata_unsorted(twixpath, 'IMG', 1);


%% Load sorted data
kdata = getkdata(twixpath, false);
kdata.data = permute(kdata.data, [1,4,2,3]);

%% Test ordering
[idx, idx_rev] = getDataOrder(kdata_unsorted);

isequal(kdata_unsorted.data(:,:,idx), kdata.data(:,:,:))

isequal(kdata_unsorted.data(:,:,:), kdata.data(:,:,idx_rev))