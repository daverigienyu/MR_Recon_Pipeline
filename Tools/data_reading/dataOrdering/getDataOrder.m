function [idx, idx_rev] = orderDataForRecon(twixobj, echoIdx)

    if ~exist('echoIdx', 'var')
        echoIdx = 1;
    end

    dims      = twixobj.image.sqzDims(end:-1:3);
    arraySize = twixobj.image.sqzSize(end:-1:3); 
    
    subInds = {};
    
    for i = 1:numel(dims)
       ii = twixobj.image.(dims{i});        
       ii = ii(twixobj.image.Eco == echoIdx);
       subInds{i} = ii;
    end
    
    idx = sub2ind(arraySize, subInds{:});
    [~, idx] = sort(idx);
    [~, idx_rev] = sort(idx);    

end