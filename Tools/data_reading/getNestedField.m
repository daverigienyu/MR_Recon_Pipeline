    function [val,successFlag] = getNestedField(S, varargin)
        
        DEFAULT_VAL = 0;
        successFlag = true;
        
        if isfield(S, varargin{1})
           val = getfield(S, varargin{1});
        else
           val = DEFAULT_VAL;
           successFlag = false;
        end
        
        if isempty(val)
            val = DEFAULT_VAL;
        end

        if numel(varargin) > 1 
            [val, successFlag] = getNestedField(val, varargin{2:end});
        end
        
    end