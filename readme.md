# MR Recon Pipeline

## Initial Setup

First check config.yml to set the correct paths. You will set paths for:

1. gpuNUFFT
2. the location of the MR Recon code (default relative paths should be fine)
3. Location of Input Data (InputData)
4. Location where reconstrution results should go (OutputResults)
5. Location where to temporarily store files while recon is in progress (WorkingTemp)

Also in config.yml, you can change the patterns that are used to match/find the various files that are required by the queue. This includes:

- top level patient folder (see below)
- MR raw data
- the gating file
- parameter file
- PET DICOM folder, used for alignment (optional)

An example is shown below:

```yaml
paths:
    mr_recon_path: ./Tools
    queue_path: ./queue.txt
    gpunufft_path: ../gpuNUFFT_Win64
    input_basepath: ../MR_Recon_Pipeline_Data/InputData
    output_basepath: ../MR_Recon_Pipeline_Data/OutputData
    workingdir_basepath: ../MR_Recon_Pipeline_Data/WorkingTemp
    
regex_match_patterns:
    patient_folder: "(([pP]atient)|([pP]hantom))[^\\.]*$"
    mr_data: (.*kdata.*((\.mat)|(\.twix))$)|(^meas.*\.dat$)
    gating_file: .*gating.*signal.*\.mat$
    parameter_file: ([pP]arm.ya?ml$)|(.*[pP]aram.*\.ya?ml$)
    pet_dicom: "[^\\.]*((PET)|(pet))[^\\.]*$"
```

## Input Data Format

The folder structure for the input data is like this:

```
Patient1_Description_whatever/
		meas_whatever.dat
		recon1/
			gating_signal_resp_5gates.mat
			recon_parameters.yml
		recon2/
			gating_signal_card_5gates.mat
			recon_parameters.yml
		recon3/
			gating_signal_respAndCard_5x5_gates.mat
			recon_parameters.yml
```

You will have a top-level directory for each scan that contains the k-space data, and a separate sub-directory for each reconstruction that you want to perform with that MR raw data, e.g. "recon1/", "recon2/", "recon3/"

### K-SPACE DATA FILE
	
the k-space data can either be a .DAT file (TWIX) or a .MAT file. If it is a .MAT file then the largest variable will be assumed to be the k-space data.

### GATING FILE
	
The gating file needs to be a .MAT file. It should have a variable called either "new_gating_signal", "LM_Gate", or "gatingSignal". It should be a 1D array of integers that specifies which motion state each acquisition belongs to OR a 2D tall array that specifies 2D gating, e.g. resp gate and card gate

## PARAMETER FILE

The parameter file should be in YAML format. Please read about YAML files for general rules about syntax. They are designed to be human-readable, so it should be straight-forward to understand upon first glance. An example of some parameters that can be set are below.

```yaml
tvWeight: 0.025
imageDim: [256, 256, 48]
nIter: 30
createGatingSignal: false           # Create discrete gating signal on the fly if you supply 'continuous' gatingSignal
nGates: []              # Only needs to be specified if 'createGatingSignal' is true
gradType: Time
tvWeightTime: 0.005
logfilepath: []              # If blank, will default to WorkingTemp then move to OutputResults upon completion
```

## Running the Queue

When everything is setup, just run the following scripts in order: 

```matlab
init; % only needs to be run once to configure paths etc.
buildQueue; % creates queue.txt listing the jobs to be processed.
submitAllJobs; % Runs the jobs in queue.txt
```

A folder will be created in the WorkingTemp directory with information
about the currently running reconstruction job. In here you will find

- a log file that displays what the queue is doing and some stats about each iteration
- a folder of "screenshots" which saves a jpeg image of one slice/gate every iteration
- a .MAT file with an intermediate reconstruction result (saved every 10 iterations)

When the job finishes, all of these files and the final result will be copied to the
OutputResults folder

When the queue is finished with all of the recon jobs in one patient data folder it
will write an empty file called "FINISHED.txt". Any data folders with this file will
be skipped, so you must delete it if you want to re-run the queue on the same patient 
data folder.



















	




