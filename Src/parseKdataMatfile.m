function [kdata, headerInfo] = parseKdataMatfile(filepath)
% Parses the .MAT file at filepath to automatically find the k-space data
% and other header info based on variable size.
%
% The largest variable is assumed to be kdata and the rest is assumed
% to be header info
%

    MF = matfile(filepath);

    fieldNames = fields(MF);
    byteArr    = zeros([1,numel(fieldNames)]);
    for i = 1:1:numel(fieldNames)
	f = fieldNames{i};
	if strcmpi(f, 'Properties');
	    continue;
	end
        I = whos(MF, f);
	byteArr(i) = I.bytes;
    end

    % Sort field names by the byte size of the variables
    [byteArr, idx] = sort(byteArr, 'descend');
    fieldNames = fieldNames(idx);

    kdata = getfield(MF, fieldNames{1});
    headerInfo = struct;
    for i = 2:1:numel(fieldNames)
        headerInfo = setfield(headerInfo, fieldNames{i}, getfield(MF, fieldNames{i}));
    end




end
