function [] = submitReconSeries(ReconSeries)

    ReconJobs = ReconSeries.ReconJobs;
        
    for i = 1:1:numel(ReconJobs)     

            job = ReconJobs(i);
            diarypath = fullfile(job.basepath, 'diary.txt');
            diary on;

            if exist(fullfile(job.basepath, 'FINISHED.txt'), 'file')
                fprintf('\nRecon Job Already Done. Skipping ...');
                continue;
            end

            % SET DEFAULT LOGFILE PATH IF NOT SPECIFIED IN PARM FILE
            if ~isfield(job.parm, 'logfilepath') || isempty(job.parm.logfilepath)
                job.parm.logfilepath = job.log_path;
            end

            [parentdir, jobdir, ~] = fileparts(job.basepath);
            
            mkdir(job.workingDir);
                    
            job.clearLog();
            
            tic;
            fprintf('\r\nSubmitting job %s...\r', jobdir);
            
            copyfile(job.gatingSignal_path, job.workingDir);
            copyfile(job.parm_path,         job.workingDir);
        
            job.log('Starting reconstruction...');
            job.log('Using kdata file %s', job.kdata_path);
            job.log('Using gating file %s', job.gatingSignal_path);
            
            twix_obj     = loadKdata(job.kdata_path);
            gatingSignal = loadGatingSignal(job.gatingSignal_path);

            % WRITE MINIHEADER TO WORKINGDIR
            job.log('Extracting miniheader info from twix_obj...');
            hdr = writeMiniHeader(twix_obj, job.workingDir);

            [im, gatingSignal]         = mrReconMain(twix_obj.data, ...
                                        gatingSignal, ...
                                        hdr, ...
                                        job.parm);
            toc;


            % FIX MR AXES TO MATCH ONLINE RECON 
            im = swapaxes(im, 1, 2); %swap first two dims
            im = flip(im, 3); %invert third dim      

            % SAVE IMAGE DATA TO MAT-FILE WITH TWIX HEADER INFO
            job.log('Saving reconstruction to MAT file');        
            reconPath = fullfile(job.workingDir, 'mr_recon.mat');
            twix_obj.data = im;
            save(reconPath, '-struct', 'twix_obj');

            %keyboard;

            % SAVE MR IMAGES TO NIFTI
            writeNiftiFiles(twix_obj, fullfile(job.workingDir, 'mr_recon_nifti'));  
            
            % REGRID MR IMAGES TO MATCH PET ONLINE RECON, SAVE AS NIFTI
            % ALSO SAVE MAT-FILE WITH ORIENTATION MATCHING INTERFILE FORMAT FOR
            % MOTION ESTIMATION
            if ~isempty(job.pet_dicom_path)
                job.log('Writing NIFTI files regridded to PET geometry');
                writeNiftiFilesPETgrid(twix_obj, job.pet_dicom_path, ... 
                        fullfile(job.workingDir, 'mr_recon_nifti_PET_grid')); 

            end
            
            % WRITE GATING SIGNAL FOR PET RECON PIPELINE
            job.log('Writing Gating Signal for PET Recon Pipeline...');
            [~,filename,~] = fileparts(job.gatingSignal_path);
            outpath = fullfile(job.workingDir, [filename '_for_PET.mat']);
            writePETGatingSignal(hdr, gatingSignal, outpath);
        
    
            % MOVE FILES TO OUTPUT DIRECTORY AND CLEANUP
            if exist(job.outputDir, 'dir')
                job.outputDir = strcat(job.outputDir, uniquestring);
            end        

            job.log('Reconstruction finished. Moving files to %s ...', job.outputDir);
            diary off;
            mkdir(job.outputDir);         
            movefile(fullfile(job.workingDir,'*'), job.outputDir);
            rmdir(job.workingDir);

            fclose(fopen(fullfile(job.basepath, 'FINISHED.txt'), 'w+'));  

    end
    
    fclose(fopen(fullfile(ReconSeries.basepath, 'FINISHED.txt'),'w+'));

end

function [twix_obj] = loadKdata(filepath)

  [filedir, filename, ext] = fileparts(filepath);

  switch upper(ext)
	  case '.DAT'
	    twix_obj = getkdata(filepath);
	  case '.MAT'
	    twix_obj = load(filepath);
      case '.TWIX'
        twix_obj = readTwix(filepath);
	  otherwise
	    error('Invalid kdata filepath');
  end

end


function gatingSignal = loadGatingSignal(filepath)

  MF = load(filepath);

  possibleFieldNames = {'new_gating_signal', 'LM_Gate', 'gatingSignal'};

  for i = 1:1:numel(possibleFieldNames)
      fieldName = possibleFieldNames{i};
      if isfield(MF, fieldName)
          gatingSignal = getfield(MF, fieldName);
          break;
      end
  end
  

  if size(gatingSignal, 2) > size(gatingSignal, 1)
      gatingSignal = gatingSignal';
  end


end


function [] = writeDicomFiles(twix_obj, basepath)

    voxelData = abs(twix_obj.data);
    
    nGate = size(voxelData(:,:,:,:), 4);

    for iGate = 1:nGate
	
		dirpath = sprintf('%s_Gate%02i', basepath, iGate);
		IV = ImageVolumeTwix(voxelData(:,:,:,iGate), twix_obj);
		try 
			IV.writeDicomStack(dirpath);	
		catch ME
			warning(ME.message);
		end

    end

end

function [] = writeNiftiFiles(twix_obj, basepath)

    voxelData = abs(twix_obj.data);
    
    nGate = size(voxelData(:,:,:,:), 4);

    for iGate = 1:nGate
	
		filepath = sprintf('%s_Gate%02i.nii', basepath, iGate);
		IV = ImageVolumeTwix(voxelData(:,:,:,iGate), twix_obj);
		try 
			IV.writeNifti(filepath);	
		catch ME
			warning(ME.message);
		end

    end

end

function [] = writeDicomFilesPETgrid(twix_obj, petpath, basepath)

    voxelData = abs(twix_obj.data);
    
    nGate = size(voxelData(:,:,:,:), 4);

    IVpet = ImageVolumeDicom(petpath);
    
    voxelDataMoco = zeros([size(IVpet.voxelData), nGate]);
    
    for iGate = 1:nGate
		dirpath = sprintf('%s_Gate%02i', basepath, iGate);
		IV  = ImageVolumeTwix(voxelData(:,:,:,iGate), twix_obj);
        IV = IV.regridvolume(IVpet); 
        try 
			IV.writeDicomStack(dirpath);	
		catch ME
			warning(ME.message);
		end
        voxelDataMoco(:,:,:,iGate) = IV.voxelData;
    end
    
    % Save mr recon data gridded for motion estimation
    twix_obj.data = flip(swapaxes(voxelDataMoco, 1, 2), 3);
    [dirpath, filename] = fileparts(basepath);
    p = fullfile(dirpath, 'mr_recon_Moco_grid.mat');
    save(p, '-struct', 'twix_obj');

end

function [] = writeNiftiFilesPETgrid(twix_obj, petpath, basepath)

    voxelData = abs(twix_obj.data);
    
    nGate = size(voxelData(:,:,:,:), 4);

    IVpet = ImageVolumeDicom(petpath);
    
    voxelDataMoco = zeros([size(IVpet.voxelData), nGate]);
    
    for iGate = 1:nGate
		filepath = sprintf('%s_Gate%02i.nii', basepath, iGate);
		IV  = ImageVolumeTwix(voxelData(:,:,:,iGate), twix_obj);
        IV = IV.regridvolume(IVpet); 
        try 
			IV.writeNifti(filepath);	
		catch ME
			warning(ME.message);
		end
        voxelDataMoco(:,:,:,iGate) = IV.voxelData;
    end
    
    % Save mr recon data gridded for motion estimation
    twix_obj.data = flip(swapaxes(voxelDataMoco, 1, 2), 3);
    [dirpath, filename] = fileparts(basepath);
    p = fullfile(dirpath, 'mr_recon_Moco_grid.mat');
    save(p, '-struct', 'twix_obj');

end

function [] = writePETGatingSignal(hdr, gatingSignal, outpath)

    col = @(x) x(:);
    
    % MAKE SURE GATING SIGNAL IS 1D
    if ~isvector(gatingSignal)
        gatingSignal = gatingSignal2Dto1D(gatingSignal);
    end
    
    N           = numel(gatingSignal);
    dt          = hdr.TR*1.0e-3; % milliseconds
    MF.LM_Time  = col(round((1:N)*dt));
    MF.LM_Gate  = gatingSignal;
    
    save(outpath, '-struct', 'MF');
    
end

function [hdr] = writeMiniHeader(twix_obj, outdir)

    hdr = createMiniHeaderFromTwix(twix_obj);
    save(fullfile(outdir, 'mini_header.mat'), '-struct', 'hdr');
    fid = fopen(fullfile(outdir, 'mini_header.txt'), 'w+');
    fprintf(fid, '%s', printnestedstruct(hdr));
    fclose(fid);
end

