classdef ReconJob < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Parent
        basepath
        kdata_path
        parm
        parm_path
        gatingSignal_path
        workingDir
        outputDir
        recon_path
        log_path
	    pet_dicom_path = [];     
   
    end
    
    methods
        function obj = ReconJob(basepath, Parent)
            
            obj.basepath          = basepath;
            obj.Parent            = Parent;

            try
                obj.parm              = obj.loadParmFile();
            catch ME
                error('Problem reading parameter file');
            end
            
            obj.kdata_path        = obj.Parent.kdata_path;
            obj.pet_dicom_path    = obj.Parent.pet_dicom_path;
            obj.gatingSignal_path = obj.findGatingSignalPath();         
            
            [~, dirname, ext] = fileparts(obj.basepath);
            obj.outputDir = fullfile(obj.Parent.outputDir, dirname);
            
            obj.workingDir = obj.Parent.workingDir;
            
            [~, kdata_basename, ext] = fileparts(obj.kdata_path);            
            obj.recon_path = fullfile(obj.workingDir, [kdata_basename '_recon' '.mat']);           
            
            obj.log_path = fullfile(obj.workingDir, 'log.txt');
            
            
            
        end
        
        function p = fullpath(obj, relpath)
           p = fullfile(obj.basepath, relpath); 
        end
        
        function parm = loadParmFile(obj)
   
            global CONFIG
            
            listing       = regexpdir(obj.basepath, CONFIG.regex_match_patterns.parameter_file);
            obj.parm_path = obj.fullpath(listing.name);
            parm          = YAML.read(obj.parm_path);
            
        end
        
        function p = findGatingSignalPath(obj)
            
           global CONFIG
            
           listing = regexpdir(obj.basepath, CONFIG.regex_match_patterns.gating_file);
           
           if numel(listing) > 1
               error('Too many gating signals found');
           elseif numel(listing) < 1
               error('No gating signal found');
           end
           
           p = listing.path
           
        end
        
        function [] = log(obj, varargin)
           msg = sprintf(varargin{:});
           fid = fopen(obj.log_path, 'a+');
           S = sprintf('\n[%s] - %s', datestr(datetime), msg);
           fprintf(fid, '%s', S);
           fprintf('%s', S);
           fclose(fid); 
        end
        
        function [] = clearLog(obj)
           fclose(fopen(obj.log_path,'w+')); 
        end
                
    end
    
    
    
end

