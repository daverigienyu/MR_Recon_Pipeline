classdef ReconSeries < handle
       
    properties
        basepath
        kdata_path
        ReconJobs
        outputDir
        log_path
        workingDir
        pet_dicom_path
    end
    
    methods
        function obj = ReconSeries(basepath, workingDir, outputDir)
            
            if nargin < 2
                outputDir = strrep(basepath, 'InputData', 'OutputResults');
            end
            
            obj.workingDir     = workingDir;
            obj.outputDir      = outputDir;
            obj.basepath       = basepath;
            obj.kdata_path     = obj.getKdataPath();
            obj.pet_dicom_path = obj.findPETDicomPath();
                      
            subdirs = obj.listSubdirs();
            
            ReconJobs = [];
            
            for i = 1:1:numel(subdirs)
                d = subdirs{i};
                if strcmpi(d, obj.pet_dicom_path)
                    continue;
                end
                ReconJobs = [ReconJobs; ReconJob(d, obj)];
            end
            
            obj.ReconJobs = ReconJobs;
            
            obj.log_path = fullfile(obj.outputDir, 'log.txt');
            
        end
        
        function p = fullpath(obj, relpath)
           p = fullfile(obj.basepath, relpath);
        end
        
        function p = getKdataPath(obj)
           global CONFIG
           listing = regexpdir(obj.basepath, CONFIG.regex_match_patterns.mr_data);
           
           if numel(listing) < 1
               error('No kdata found.');
           end
           
           if numel(listing) > 1
              error('Too many kdata files found!'); 
           end
           
           p       = listing.path
        end
        
        function p = findPETDicomPath(obj)
          global CONFIG
          % default value;
          p = [];
          
          filelist = regexpdir(obj.basepath, CONFIG.regex_match_patterns.pet_dicom, true, ... 
                               'ignoreCase', true);
                           
          for i = 1:numel(filelist)
              pi = filelist(i).path;
              if exist(pi, 'dir')
                  p = pi;
                  return;
              end   
          end
          
        end
                
        function subdirs = listSubdirs(obj)
            listing = dir(obj.basepath);
            listing = listing(3:end);
            listing = listing([listing.isdir]);
            subdirs = obj.fullpath({listing.name});
        end
        
        function [] = log(obj, varargin)
           msg = sprintf(varargin{:});
           fid = fopen(obj.log_path, 'a+');
           S = sprintf('\n[%s] - %s\r', datestr(datetime), msg);
           fprintf(fid, S);
           fprintf(S);
           fclose(fid); 
        end
        
        function [] = clearLog(obj)
           fclose(fopen(obj.log_path,'w+')); 
        end
    end
    
end
